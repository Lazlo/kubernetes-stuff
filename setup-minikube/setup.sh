#!/bin/bash

# Install libvirt (which automatically installs qemu-kvm)
sudo apt-get -q install -y libvirt-daemon
# Install kubectl
sudo apt-get -q install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
# NOTE Even though we are using Debian we leave the "dist" as it is.
sudo cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get -q install -y kubectl
# Install minikube
wget https://github.com/kubernetes/minikube/releases/download/v1.2.0/minikube_1.2.0.deb
sudo dpkg -i minikube_1.2.0.deb
rm minikube_1.2.0.deb
# Install docker-machine-driver-kvm2
https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2
chmod +x docker-machine-driver-kvm2
sudo chown root:root docker-machine-driver-kvm2
sudo mv docker-machine-driver-kvm2 /usr/local/bin
