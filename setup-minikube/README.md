# Kubernetes Setup using minikube

## Setup

See and then run the ```setup.sh``` file

## Starting

```
minikube start --vm-driver kvm2
```

This command will use libvirt to create a virtual machine. Inside this VM minikube is running.

In case starting minikube fails ...

 * and it complained about not being able to start the domain, do the following
   - delete the .minikube directory of your user and start minikube again (```rm -rf ~/.minikube```)

When minikube has finished creating the VM,, run this command to verify it is running:

```
minikube ip
```
